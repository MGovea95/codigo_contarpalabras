
package com;


public class Palabra 
{
   private String oalabra;
   private int frecuencia;

   public Palabra(String nom,int num)
   {
       this.oalabra=nom;
       this.frecuencia=num;
   }
   
    public String getOalabra() {
        return oalabra;
    }

    public void setOalabra(String oalabra) {
        this.oalabra = oalabra;
    }

    public int getFrecuencia() {
        return frecuencia;
    }

    public void setFrecuencia(int frecuencia) {
        this.frecuencia = frecuencia;
    }
    
    public String toString()
    {
        return this.oalabra+" : "+this.frecuencia;
    }
    
}
